public class RectangleApp {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5, "Rect1");
        rect1.printAreaRectangle();
        rect1.printPerimeterRectangle();
        Rectangle rect2 = new Rectangle(5, 3, "Rect2");
        rect2.printAreaRectangle();
        rect2.printPerimeterRectangle();
    }
}
