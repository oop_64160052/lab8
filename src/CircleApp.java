public class CircleApp {
    public static void main(String[] args) {
        Circle circle1 = new Circle(1, "Circle1");
        circle1.printAreaCircle();
        circle1.printPerimeterCircle();

        Circle circle2 = new Circle(2, "Circle2");
        circle2.printAreaCircle();
        circle2.printPerimeterCircle();

    }
}
