public class Circle {
    private double radian;
    private String name;

    public Circle(int radian, String name) {
        this.radian = radian;
        this.name = name;
    }

    public int printAreaCircle() { // Construtor
        double area = 3.14 * (radian * radian);
        System.out.println(name + " area = " + area);
        return (int) area;
    }

    public int printPerimeterCircle() {
        double perimeter = 2 * 3.14 * radian;
        System.out.println(name + " perimeter = " + perimeter);
        return (int) perimeter;
    }
}
