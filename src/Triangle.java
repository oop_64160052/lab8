import java.math.*;

public class Triangle {
    // Attributes
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) { // Constructor
        this.a = a;
        this.b = b;
        this.c = c;
    }

    // Methodes
    public double printAreaTriangle() {
        double x = (a + b + c) / 2;
        double area = Math.sqrt(x * (x - a) * (x - b) * (x - c));
        System.out.println("Calculate area = " + area);
        return area;

    }

    public double printPerimeterTriangle() {
        double perimeter = a + b + c;
        System.out.println("Calculate perimeter = " + perimeter);
        return perimeter;
    }

}
