public class TestTree {
        private int x;
        private int y;
        private String name;
    
        public TestTree(int x, int y, String name){
            this.x = x;
            this.y = y;
            this.name = name;
        }

        public void print(){
            System.out.println(name + "  x: " + x + "  y: "+y);
        }
}
