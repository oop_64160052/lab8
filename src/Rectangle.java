public class Rectangle {
    private int width;
    private int height;
    private String name;

    public Rectangle(int width, int height, String name){
        this.width = width;
        this.height = height;
        this.name = name;

    }
    public int printAreaRectangle(){
        int area = height * width;
        System.out.println(name + " area = " + area);
        return area;
    }
    public int printPerimeterRectangle(){
        int perimeter = (height + width) * 2;
        System.out.println(name + " perimeter = " + perimeter);
        return perimeter;
    }
}
